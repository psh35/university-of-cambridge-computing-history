# University of Cambridge Computing History

An attempt to gather links to the University of Cambridge computing history.

## Jackdaw

- https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-1.html
- https://confluence.uis.cam.ac.uk/display/JAC/Jackdaw+History

## Raven (Ucam-webauth)
- https://raven.cam.ac.uk/project/
- https://github.com/cambridgeuniversity/UcamWebauth-protocol
- https://github.com/cambridgeuniversity/mod_ucam_webauth


## Philip Hazel 

- Wikipedia: https://en.wikipedia.org/wiki/Philip_Hazel
- Autobiography: https://drive.google.com/file/d/10TAOEgIL--CmzqOl0fkdP-cagjSLBM4J/view?usp=sharing
- BBC Micro game (Whack a mole): http://bbcmicro.co.uk/game.php?id=2967

## Generic

- https://cucps.soc.srcf.net/

